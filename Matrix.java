class Matrix {

  private int[][] user;
  //the matrix constructor makes a matrix of 0's
    public Matrix(int rows, int cols)
    {
      user=new int[rows][cols];
    }

	 /*This function takes in a multidimensional array and returns its determinant.
    the input matrix must be a square matrix.
    The method calculates the determinant recursively.
   */
  public int realdet(int[][] mat)
  {
    int size=mat[0].length;
    //the base case********************
    if(size==2)
    {
      return mat[0][0]*mat[1][1]-mat[0][1]*mat[1][0];
    }

    else
    {
      int det=0;
      //add to the determinant by expanding along the "0" row
      for(int col=0;col<mat[0].length;col++)
      //if we expand only on the "0" row, we must alternate the +/- sign according to whether the column 
      	//we're expanding on is odd (negative) or even (positive)
      {
        int addthis=mat[0][col]*realdet(submatrix(mat,0,col));
        det+= (col%2==1) ? -addthis: addthis;
      }
      return det;
    }


    
  }

//this method takes a matrix and a row number and column number. These 
  //indicate which row and column to "ignore"; the submatrix is then 
  //made of the original rows and columns, without the indicated row
  //and column.
  public int[][] submatrix(int[][] mat, int row, int col)
  {
    int [][] submat=new int[mat[0].length-1][mat[0].length-1];
     
    int rowpos=0;
    int colpos=0;
    for(int r=0;r<mat[0].length;r++)
    {
      for(int c=0;c<mat[0].length;c++)
      {
        //if we aren't on a row or column we're igoring, then read it into our
        //new submatrix and move forward a column
        if(!(r==row || c==col))
        {
          submat[rowpos][colpos]=mat[r][c];
          colpos++;
          if(colpos==mat[0].length-1)
          {
            colpos=0;
            rowpos++;
          }
        }
      }
    }
   
    return submat;
  }

//determines if a square matrix is symmetric by checking 
public boolean symmetric(int[][] matrix)
{
  boolean symmetry=true;
    for(int r=0;r<matrix[0].length;r++)
    {
      for(int c=0;c<matrix[0].length;c++)
      {
      	if(r!=c) //don't need to check the diagonal entries, might as well
      		//skip them and maybe save a little time
      	  { 
      	  	if(matrix[r][c]!=matrix[c][r])
        	  symmetry=false;
          }
      }
    }
    return symmetry;
 } 
}

